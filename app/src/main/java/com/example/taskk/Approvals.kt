package com.example.taskk

import com.google.gson.annotations.SerializedName

data class Approvals (

    @SerializedName("id"   ) var id   : String? = null,
    @SerializedName("email"    ) var email    : String? = null,
    @SerializedName("newpassword"  ) var newpassword  : String? = null,
    @SerializedName("newname"  ) var newname  : String? = null,
    @SerializedName("newnumber"      ) var newnumber      : String? = null,
    @SerializedName("newcity"  ) var newcity  : String? = null,
    @SerializedName("newdob"    ) var newdob    : String? = null,
    @SerializedName("oldpassword"  ) var oldpassword  : String? = null,
    @SerializedName("oldname"  ) var oldname  : String? = null,
    @SerializedName("oldnumber"      ) var oldnumber      : String? = null,
    @SerializedName("oldcity"  ) var oldcity  : String? = null,
    @SerializedName("olddob"    ) var olddob    : String? = null,
    @SerializedName("accountstatus"    ) var accountstatus    : String? = null,
    @SerializedName("profilestatus" ) var profilestatus : String? = null,
    @SerializedName("emptype" ) var emptype : String? = null,
    @SerializedName("createddate" ) var createddate : String? = null


)