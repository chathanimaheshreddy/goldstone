package com.example.taskk

import android.annotation.SuppressLint
import android.os.StrictMode
import android.util.Log
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException

class ConnectionClass {
    var classs = "com.mysql.jdbc.Driver"
    var url = "jdbc:mysql://192.168.0.176/user"
    //var url = "jdbc:mysql://192.168.26.232/user"
    var un = "test"
    var password = "test"
    @SuppressLint("NewApi")
    fun CONN(): Connection? {
        val policy = StrictMode.ThreadPolicy.Builder()
            .permitAll().build()
        StrictMode.setThreadPolicy(policy)
        var conn: Connection? = null
        val ConnURL: String? = null
        try {
            Class.forName(classs)
            conn = DriverManager.getConnection(url, un, password)
            conn = DriverManager.getConnection(ConnURL)
        } catch (se: SQLException) {
            Log.e("ERRO", se.message!!)
        } catch (e: ClassNotFoundException) {
            Log.e("ERRO", e.message!!)
        } catch (e: Exception) {
            Log.e("ERRO", e.message!!)
        }
        return conn
    }
}