package com.example.taskk

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.sql.Time
import java.util.Calendar
import java.util.Date

class DBHelper(context: Context, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {

        val query = ("CREATE TABLE " + TABLE_NAME + " ("
                + ID_COL + " INTEGER PRIMARY KEY, " +
                NAME_COl + " TEXT, " +
                MOBILE+ " TEXT, " +
                EMAIL+ " TEXT, " +
                DOB+ " TEXT, " +
                CITY+ " TEXT, " +
                PASSWORD+ " TEXT, " +
                STATUS+ " TEXT, " +
                TYPE+ " TEXT, " +
                CREATEDATE+ " TEXT" +")")
        db.execSQL(query)
    }

    override fun onUpgrade(db: SQLiteDatabase, p1: Int, p2: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    fun registeruser(name : String, mobile : String, email : String, dob : String
                     , city : String, password : String){
        val values = ContentValues()
        values.put(NAME_COl, name)
        values.put(MOBILE, mobile)
        values.put(EMAIL, email)
        values.put(DOB, dob)
        values.put(CITY, city)
        values.put(PASSWORD, password)
        values.put(STATUS, "pending")
        values.put(TYPE, "user")
        values.put(CREATEDATE, System.currentTimeMillis())
        val db = this.writableDatabase
        db.insert(TABLE_NAME, null,values)
        db.close()
    }
    fun updateage(name : String, mobile : String, email : String, dob : String
                  , city : String, password : String ){
        val values = ContentValues()
        values.put(NAME_COl, name)
        values.put(MOBILE, mobile)
        values.put(EMAIL, email)
        values.put(DOB, dob)
        values.put(CITY, city)
        values.put(PASSWORD, password)
        val db = this.writableDatabase
      //  db.update(TABLE_NAME, values, AGE_COL, arrayOf(name))
        db.update(TABLE_NAME, values, "$NAME_COl=?", arrayOf(name));
        db.close()
    }
    fun getdata(): Cursor? {
        val db = this.readableDatabase
        return db.rawQuery("SELECT * FROM $TABLE_NAME", null)

    }
    fun checkusermail(email : String): Boolean? {
        val db = this.readableDatabase
        val cursor = db.rawQuery("SELECT * FROM $TABLE_NAME WHERE $EMAIL = '$email'", null)
        return cursor!=null && cursor.getCount()>0

    }
    fun checklogin(email: String ,password: String ): String? {
        val db = this.readableDatabase
        val cursor =  db.rawQuery("SELECT * FROM $TABLE_NAME WHERE $EMAIL = '$email'", null)
        if(cursor!=null && cursor.getCount()>0){
            val cursor1 =  db.rawQuery("SELECT * FROM $TABLE_NAME WHERE $EMAIL = '$email' AND $PASSWORD = '$password'", null)
            if(cursor1!=null && cursor1.getCount()>0){
                return "Login Success";
            }else{
                return "Invalid Username or Password!"
            }
        }else{
            return "User not available!"
        }

    }

    companion object{
        private val DATABASE_NAME = "goldstone"
        private val DATABASE_VERSION = 1
        val TABLE_NAME = "user_table"
        val ID_COL = "id"
        val NAME_COl = "name"
        val MOBILE = "mobile"
        val EMAIL = "email"
        val DOB = "dob"
        val CITY = "city"
        val PASSWORD = "password"
        val STATUS = "status"
        val TYPE = "type"
        val CREATEDATE = "date"
    }
}