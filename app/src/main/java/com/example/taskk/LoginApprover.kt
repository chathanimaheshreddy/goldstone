package com.example.taskk

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import java.sql.Connection
import java.sql.ResultSet


class LoginApprover : AppCompatActivity() {
    var progressDialog: ProgressDialog? = null
    @SuppressLint("Range")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loginap)
        var sharedPreference =  getSharedPreferences("PREFERENCE_APPROVER", Context.MODE_PRIVATE)
        var  islogin: Boolean? = sharedPreference.getBoolean("login",false)
        if (islogin == true){
            moveusertodahsboard()
        }
        progressDialog = ProgressDialog(this@LoginApprover)
        var loginbtn:Button = findViewById(R.id.loginbtn)
        var email:EditText = findViewById(R.id.email)
        var password:EditText = findViewById(R.id.password)


        loginbtn.setOnClickListener {
            var emailtxt: String
            var passwordtxt: String

            if (email.text.length == 0){
                email.setError("Enter email")
                email.requestFocus()
            }else if (password.text.length == 0){
                password.setError("Enter password")
                password.requestFocus()
            }else if (!isValidEmail(email.text.toString())){
                email.setError("Enter valid email")
                email.requestFocus()
            }else{
                //userLogin(email.text.toString(),password.text.toString())
                val dologin = Dologin()
                dologin.execute(email.text.toString(),AESEncyption.encrypt(password.text.toString().trim()))


            }
        }
    }


    private fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    inner class Dologin : AsyncTask<String?, String?, String?>() {

        var connectionClass: ConnectionClass? = ConnectionClass()
        var z = ""
        var isSuccess = false
        var email: String? = null
        var password: String? = null
        var name: String? = null
        var accountStatus: String? = null
        var type: String? = null

        override fun onPreExecute() {

            progressDialog!!.setMessage("Loading...")
            progressDialog!!.show()
        }

        override fun doInBackground(vararg params: String?): String {

            try {
                val con: Connection = connectionClass!!.CONN()!!
                if (con == null) {
                    z = "Please check your internet connection"
                } else {
                    val query1 =
                        " select * from usertable where email='${params[0]}' and emptype='approver'"
                    val stmt1 = con.createStatement()
                    // stmt.executeUpdate(query);
                    val rs: ResultSet = stmt1.executeQuery(query1)
                    var i = 0
                    while (rs.next()) {
                        i++
                    }
                    if (i > 0){
                        val query =
                            " select * from usertable where email='${params[0]}' and oldpassword='${params[1]!!.trim()}' and emptype='approver'"
                        val stmt = con.createStatement()
                        // stmt.executeUpdate(query);
                        val rs: ResultSet = stmt.executeQuery(query)
                        var j = 0
                        while (rs.next()) {
                            j++
                            email=rs.getString(2);
                            password=rs.getString(8);
                            name=rs.getString(9);
                            accountStatus=rs.getString(13);
                            type=rs.getString(15);
                            Log.d("TAG", "doInBackground:aaaaa "+email+"-"+name+"-"+accountStatus)
                            if(email.equals(params[0])&&password.equals(params[1]!!.trim())) {
                                if (accountStatus == "pending"){
                                    isSuccess = false;
                                    z = "your account is in approval proccess"
                                }else  if (accountStatus == "rejected"){
                                    isSuccess = false;
                                    z = "your account is rejected.."
                                }else{
                                    isSuccess=true;
                                    z = "Login successfull";
                                }

                            } else {
                                isSuccess = false;
                                z = "invalid password"
                            }
                        }
                        if (j == 0){
                            isSuccess = false;
                            z = "invalid password"
                        }

                    }else{
                        isSuccess = false
                        z = "User not found"
                    }

                }
            } catch (ex: Exception) {
                isSuccess = false
                z = "Exceptions$ex"
            }

            return z
        }

        override fun onPostExecute(s: String?) {
            if (isSuccess){
                var sharedPreference =  getSharedPreferences("PREFERENCE_APPROVER", Context.MODE_PRIVATE)
                var editor = sharedPreference!!.edit()
                editor.putString("name",name)
                editor.putString("email",email)
                editor.putString("type",type)
                editor.putBoolean("login",true)
                editor.commit()
                moveusertodahsboard()

            }
            Toast.makeText(this@LoginApprover, "" + z, Toast.LENGTH_LONG).show()
            Log.d("TAG", "onPostExecute: tttttttttttttt"+z.toString())
            progressDialog!!.hide()
        }
    }

    private fun moveusertodahsboard() {
        val intent = Intent()
        intent.setClass(this@LoginApprover,MainActivity2::class.java)
        startActivity(intent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (progressDialog != null ){
            progressDialog!!.dismiss()
            progressDialog = null
        }
    }
}