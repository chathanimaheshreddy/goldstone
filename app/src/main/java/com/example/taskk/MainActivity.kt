package com.example.taskk

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var textview: TextView = findViewById(R.id.textview)
        var editbtn: Button = findViewById(R.id.edit)
        var logoutbtn: Button = findViewById(R.id.logout)
        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle("Dashboard")
        val sharedPreference =  getSharedPreferences("PREFERENCE_USER", Context.MODE_PRIVATE)
        var  name: String? = sharedPreference.getString("name","")
        var  email: String? = sharedPreference.getString("email","")
        var  type: String? = sharedPreference.getString("type","")
        
        textview.text = "Welcome $name !"

        editbtn.setOnClickListener {
            val intent = Intent()
            intent.setClass(this@MainActivity,profileedit::class.java)
            startActivity(intent)

        }
        logoutbtn.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Alert!")
            builder.setMessage("Are sure you want to Logout ?")
            builder.setPositiveButton("Yes") { dialogInterface, which ->
                var sharedPreference =  getSharedPreferences("PREFERENCE_USER", Context.MODE_PRIVATE)
                var editor = sharedPreference!!.edit()
                editor.clear()
                editor.putBoolean("login",false)
                editor.commit()
                val intent = Intent()
                intent.setClass(this@MainActivity,LoginUser::class.java)
                startActivity(intent)
                finish()
            }
            builder.setNegativeButton("No") { dialogInterface, which ->
                dialogInterface.dismiss()
            }
            val alertDialog: AlertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home){
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}