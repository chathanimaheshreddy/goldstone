package com.example.taskk

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.sql.Connection
import java.sql.ResultSet
import java.util.ArrayList

class MainActivity2 : AppCompatActivity() {
    var progressDialog: ProgressDialog? = null
    var approvalsList = ArrayList<Approvals>()
    var rec: RecyclerView? = null
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        var textview: TextView = findViewById(R.id.textview)
        var heading: TextView = findViewById(R.id.heading)
        var logoutbtn: Button = findViewById(R.id.logout)
        rec = findViewById(R.id.rec)
        progressDialog = ProgressDialog(this@MainActivity2)
        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle("Dashboard")
        val sharedPreference =  getSharedPreferences("PREFERENCE_APPROVER", Context.MODE_PRIVATE)
        var  name: String? = sharedPreference.getString("name","")
        var  email: String? = sharedPreference.getString("email","")
        var  type: String? = sharedPreference.getString("type","")

        textview.text = "Welcome $name !"

        logoutbtn.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Alert!")
            builder.setMessage("Are sure you want to Logout ?")
            builder.setPositiveButton("Yes") { dialogInterface, which ->
                var sharedPreference =  getSharedPreferences("PREFERENCE_APPROVER", Context.MODE_PRIVATE)
                var editor = sharedPreference!!.edit()
                editor.clear()
                editor.putBoolean("login",false)
                editor.commit()
                val intent = Intent()
                intent.setClass(this@MainActivity2,LoginApprover::class.java)
                startActivity(intent)
                finish()
            }
            builder.setNegativeButton("No") { dialogInterface, which ->
                dialogInterface.dismiss()
            }
            val alertDialog: AlertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
        heading.setOnClickListener {
            val updateHandler = Handler()
            val runnable = Runnable {
                val getApprovals = getApprovals()
                getApprovals.execute()
            }
            updateHandler.postDelayed(runnable,1000)
        }
        val getApprovals = getApprovals()
        getApprovals.execute()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home){
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
    inner class getApprovals : AsyncTask<String?, String?, String?>() {

        var connectionClass: ConnectionClass? = ConnectionClass()
        var z = ""
        var isSuccess = false

        override fun onPreExecute() {

            progressDialog!!.setMessage("Loading...")
            progressDialog!!.show()
        }

        override fun doInBackground(vararg params: String?): String {

            try {
                val con: Connection = connectionClass!!.CONN()!!
                if (con == null) {
                    z = "Please check your internet connection"
                } else {
                    val query1 =
                        " select * from usertable where emptype='user' and accountstatus='pending' or profilestatus='pending'"
                    val stmt1 = con.createStatement()
                    // stmt.executeUpdate(query);
                    val rs: ResultSet = stmt1.executeQuery(query1)
                    approvalsList.clear()
                    while (rs.next()) {
                        var approvals = Approvals()
                        approvals.id = rs.getString(1)
                        approvals.email = rs.getString(2)
                        approvals.newpassword = rs.getString(3)
                        approvals.newname = rs.getString(4)
                        approvals.newnumber = rs.getString(5)
                        approvals.newcity = rs.getString(6)
                        approvals.newdob = rs.getString(7)
                        approvals.oldpassword = rs.getString(8)
                        approvals.oldname = rs.getString(9)
                        approvals.oldnumber = rs.getString(10)
                        approvals.oldcity = rs.getString(11)
                        approvals.olddob = rs.getString(12)
                        approvals.accountstatus = rs.getString(13)
                        approvals.profilestatus = rs.getString(14)
                        approvals.emptype = rs.getString(15)
                        approvals.createddate = rs.getString(16)
                        approvalsList.add(approvals)


                    }
                    if (approvalsList.size > 0){
                        Log.d("TAG", "doInBackground: ")
                        isSuccess = true
                        z = "${approvalsList.size} records found"
                    }else{

                        isSuccess = false
                        z = "no approvals found"
                    }

                }
            } catch (ex: Exception) {
                isSuccess = false
                z = "Exceptions$ex"
            }

            return z
        }

        override fun onPostExecute(s: String?) {
            if (isSuccess){
                runOnUiThread {
                    rec!!.visibility = View.VISIBLE
                    rec!!.layoutManager = LinearLayoutManager(this@MainActivity2)
                    val adapter = CustomAdapter(approvalsList)
                    rec!!.adapter = adapter
                }
            }else{
                runOnUiThread {
                    rec!!.visibility = View.GONE
                }
            }
            Toast.makeText(this@MainActivity2, "" + z, Toast.LENGTH_LONG).show()
            Log.d("TAG", "onPostExecute: tttttttttttttt"+z.toString())
            progressDialog!!.hide()
        }
    }

    inner class CustomAdapter(private val mList: java.util.ArrayList<Approvals>): RecyclerView.Adapter<CustomAdapter.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.invoicelistitem, parent, false)
            return ViewHolder(view)
        }
        @SuppressLint("SetTextI18n")
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val approval: Approvals = mList[position]
            holder.name.text = approval.oldname
            holder.email.text = approval.email
            holder.number.text = approval.oldnumber
            holder.dob.text = approval.olddob
            holder.city.text = approval.oldcity
            if (approval.accountstatus.equals("pending")){
                holder.approvaltype.text = "Account"
            }else{
                holder.approvaltype.text = "Profile Edit"
            }
            holder.approve.setOnClickListener {
                val builder = AlertDialog.Builder(this@MainActivity2)
                builder.setTitle("Alert!")
                builder.setMessage("Are sure you want to Approve ?")
                builder.setPositiveButton("Yes") { dialogInterface, which ->
                    approveorrejectitem(approval,holder.approvaltype.text.toString(),true)
                }
                builder.setNegativeButton("No") { dialogInterface, which ->
                    dialogInterface.dismiss()
                }
                val alertDialog: AlertDialog = builder.create()
                // Set other dialog properties
                alertDialog.setCancelable(false)
                alertDialog.show()

            }
            holder.reject.setOnClickListener {
                val builder = AlertDialog.Builder(this@MainActivity2)
                builder.setTitle("Alert!")
                builder.setMessage("Are sure you want to Reject ?")
                builder.setPositiveButton("Yes") { dialogInterface, which ->
                    approveorrejectitem(approval,holder.approvaltype.text.toString(),false)
                }
                builder.setNegativeButton("No") { dialogInterface, which ->
                    dialogInterface.dismiss()
                }
                val alertDialog: AlertDialog = builder.create()
                // Set other dialog properties
                alertDialog.setCancelable(false)
                alertDialog.show()

            }

        }
        override fun getItemCount(): Int {
            return mList.size
        }
        inner class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
            val name: TextView = itemView.findViewById(R.id.name)
            val approvaltype: TextView = itemView.findViewById(R.id.approvaltype)
            val email: TextView = itemView.findViewById(R.id.email)
            val number: TextView = itemView.findViewById(R.id.number)
            val dob: TextView = itemView.findViewById(R.id.dob)
            val city: TextView = itemView.findViewById(R.id.city)
            val approve: Button = itemView.findViewById(R.id.approve)
            val reject: Button = itemView.findViewById(R.id.reject)
        }
    }

    private fun approveorrejectitem(approval: Approvals, type: String, approve: Boolean) {
        var approvestring: String? = null
        var typestring: String? = null
        var fullstring: String? = null
        if (approve){
            approvestring = "approved"
        }else{
            approvestring = "rejected"
        }
        if (type.equals("Account")){
            typestring = "accountstatus"
            fullstring = "$typestring='$approvestring'"
        }else{
            typestring = "profilestatus"
            if (approve) {
                fullstring = "$typestring='$approvestring',oldpassword='${approval.newpassword}',oldname='${approval.newname}',oldnumber='${approval.newnumber}',olddob='${approval.newdob}',oldcity='${approval.newcity}'"
            }else{
                fullstring = "$typestring='$approvestring',newpassword='${approval.oldpassword}',newname='${approval.oldname}',newnumber='${approval.oldnumber}',newdob='${approval.olddob}',newcity='${approval.oldcity}'"
            }
        }

        val approverejecttask = approverejecttask()
        approverejecttask.execute(fullstring,approvestring,approval.email)

    }
    inner class approverejecttask : AsyncTask<String?, String?, String?>() {

        var connectionClass: ConnectionClass? = ConnectionClass()
        var z = ""
        var isSuccess = false

        override fun onPreExecute() {

            progressDialog!!.setMessage("Loading...")
            progressDialog!!.show()
        }

        override fun doInBackground(vararg params: String?): String {

            try {
                val con: Connection = connectionClass!!.CONN()!!
                if (con == null) {
                    z = "Please check your internet connection"
                } else {
                    val query1 =
                        " update usertable set ${params[0]} where emptype='user' and email='${params[2]}'"
                    val stmt1 = con.createStatement()
                    val rs: Int = stmt1.executeUpdate(query1);
                    //val rs: ResultSet = stmt1.executeQuery(query1)
                    Log.d("TAG", "doInBackground: "+rs)
                    if (rs > 0){
                        isSuccess = true
                        z = params[1].toString()+ " successfully"
                    }else{
                        isSuccess = false
                        z = " Action failed "
                    }

                }
            } catch (ex: Exception) {
                isSuccess = false
                z = "Exceptions$ex"
            }

            return z
        }

        override fun onPostExecute(s: String?) {
            if (isSuccess){
                val updateHandler = Handler()
                val runnable = Runnable {
                    val getApprovals = getApprovals()
                    getApprovals.execute()
                }
                updateHandler.postDelayed(runnable,1000)

            }
            Toast.makeText(this@MainActivity2, "" + z, Toast.LENGTH_SHORT).show()
            Log.d("TAG", "onPostExecute: tttttttttttttt"+z.toString())
            progressDialog!!.hide()
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        if (progressDialog != null ){
            progressDialog!!.dismiss()
            progressDialog = null
        }
    }
}
