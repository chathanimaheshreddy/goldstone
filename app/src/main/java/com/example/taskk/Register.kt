package com.example.taskk

import android.R.attr.password
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.os.AsyncTask
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.taskk.AESEncyption
import com.example.taskk.ConnectionClass
import com.example.taskk.DBHelper
import com.example.taskk.R
import java.sql.Connection
import java.sql.ResultSet
import java.sql.Statement
import java.util.*


class Register : AppCompatActivity() {
    private val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+.+[a-z]+"
    var progressDialog: ProgressDialog? = null
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        progressDialog = ProgressDialog(this@Register)

        var regbtn: Button = findViewById(R.id.regbtn)
        var email: EditText = findViewById(R.id.email)
        var name: EditText = findViewById(R.id.name)
        var mobile: EditText = findViewById(R.id.number)
        var dob: EditText = findViewById(R.id.dob)
        var city: EditText = findViewById(R.id.city)
        var password: EditText = findViewById(R.id.password)
        var repassword: EditText = findViewById(R.id.repassword)

        dob.setOnFocusChangeListener { view, b ->
            if (b){
                val c = Calendar.getInstance()
                val year = c.get(Calendar.YEAR)
                val month = c.get(Calendar.MONTH)
                val day = c.get(Calendar.DAY_OF_MONTH)
                val datePickerDialog = DatePickerDialog(
                    this,
                    { view, year, monthOfYear, dayOfMonth ->
                        val dat = (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                        dob.setText(dat)
                        dob.clearFocus()
                    },
                    year,
                    month,
                    day
                )
                datePickerDialog.show()
                val calendar = Calendar.getInstance()
                calendar.add(Calendar.YEAR, -18)
                datePickerDialog.datePicker.maxDate = calendar.timeInMillis
            }
        }
        //val db = DBHelper(this, null)
        regbtn.setOnClickListener {
            if (name.text.isEmpty()){
                name.setError("Please Enter Name")
                name.requestFocus()
            }else if (mobile.text.isEmpty()){
                mobile.setError("Please Enter Mobile Number")
                mobile.requestFocus()
            }else if (mobile.text.length != 10){
                mobile.setError("Mobile Number should be 10 digits")
                mobile.requestFocus()
            }else  if (email.text.isEmpty()){
                email.setError("Please Enter Email Address")
                email.requestFocus()
            }else if (!isValidEmail(email.text.toString())) {
                email.setError("Please valid email")
                email.requestFocus()
            }else if (dob.text.isEmpty()){
                dob.setError("Please select dob")
                dob.requestFocus()
            }else if (city.text.isEmpty()){
                city.setError("Please enter city")
                city.requestFocus()
            }else if (password.text.isEmpty()){
                password.setError("Please enter password")
                password.requestFocus()
            }else if (repassword.text.isEmpty()){
                repassword.setError("Please enter confirm password")
                repassword.requestFocus()
            }else if (!isValidPassword(password.text.toString())){
                password.setError("Enter valid password")
            }else if (!isValidPassword(repassword.text.toString())){
                repassword.setError("Enter valid confirm password")
            }else if (password.text.toString() != repassword.text.toString()){
                repassword.setError("password not match")
                repassword.requestFocus()
            }else{

               /* db.registeruser(name.text.toString() , mobile.text.toString() , email.text.toString() , dob.text.toString()
                    , city.text.toString() , password.text.toString() )
                Toast.makeText(this,"user created successfully", Toast.LENGTH_LONG).show()*/
                val doregister = Doregister()
                doregister.execute(email.text.toString(),AESEncyption.encrypt(password.text.toString()),name.text.toString(),mobile.text.toString(),
                    city.text.toString(),dob.text.toString(),AESEncyption.encrypt(password.text.toString()),name.text.toString(),mobile.text.toString(),
                    city.text.toString(),dob.text.toString())


            }

        }

    }
    private fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
    internal fun isValidPassword(password: String): Boolean {
        if (password.length < 8) return false
        if (password.filter { it.isDigit() }.firstOrNull() == null) return false
        if (password.filter { it.isLetter() }.filter { it.isUpperCase() }.firstOrNull() == null) return false
        if (password.filter { it.isLetter() }.filter { it.isLowerCase() }.firstOrNull() == null) return false
        if (password.filter { !it.isLetterOrDigit() }.firstOrNull() == null) return false

        return true
    }
    inner class Doregister : AsyncTask<String?, String?, String?>() {

        var connectionClass: ConnectionClass? = ConnectionClass()
        var z = ""
        var isSuccess = false

         override fun onPreExecute() {

            progressDialog!!.setMessage("Loading...")
            progressDialog!!.show()
        }

        override fun doInBackground(vararg params: String?): String {
                try {
                    val con: Connection = connectionClass!!.CONN()!!
                    if (con == null) {
                        z = "Please check your internet connection"
                    } else {
                        val query1 =
                            " select * from usertable where email='${params[0]}'"
                        val stmt1 = con.createStatement()
                        // stmt.executeUpdate(query);
                        val rs: ResultSet = stmt1.executeQuery(query1)
                        var i = 0
                        while (rs.next()) {
                          i++
                        }
                        if (i == 0){
                            val query =
                                "insert into usertable values(null,'${params[0]}','${params[1]!!.trim()}','${params[2]}','${params[3]}','${params[4]}'" +
                                        ",'${params[5]}','${params[6]!!.trim()}','${params[7]}','${params[8]}','${params[9]}','${params[10]}','pending','approved','user',null)"
                            val stmt: Statement = con.createStatement()
                            stmt.executeUpdate(query)
                            z = "Register successfull"
                            isSuccess = true
                        }else{
                            isSuccess = false
                            z = "User already exists"
                        }

                    }
                } catch (ex: Exception) {
                    isSuccess = false
                    z = "Exceptions$ex"
                }

            return z
        }

         override fun onPostExecute(s: String?) {
             if (isSuccess){
                 onBackPressed()
             }
            Toast.makeText(this@Register, "" + z, Toast.LENGTH_LONG).show()
             Log.d("TAG", "onPostExecute: tttttttttttttt"+z.toString())
            progressDialog!!.hide()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (progressDialog != null ){
            progressDialog!!.dismiss()
            progressDialog = null
        }
    }

}