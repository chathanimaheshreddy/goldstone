package com.example.taskk

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Context
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import java.sql.Connection
import java.sql.ResultSet
import java.sql.Statement
import java.util.*

class profileedit : AppCompatActivity() {
    var updtbtn: Button? = null
    var email: EditText? = null
    var name: EditText? =null
    var mobile: EditText? = null
    var dob: EditText? = null
    var city: EditText? = null
    var password: EditText? = null
    var repassword: EditText? = null
    var progressDialog: ProgressDialog? = null
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profileedit)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle("Edit Profile")
        progressDialog = ProgressDialog(this@profileedit)
        val sharedPreference =  getSharedPreferences("PREFERENCE_USER", Context.MODE_PRIVATE)
        var  username: String? = sharedPreference.getString("name","")
        var  emailid: String? = sharedPreference.getString("email","")



         updtbtn = findViewById(R.id.updatebtn)
         email = findViewById(R.id.email)
         name = findViewById(R.id.name)
         mobile = findViewById(R.id.number)
         dob = findViewById(R.id.dob)
         city = findViewById(R.id.city)
         password = findViewById(R.id.password)
         repassword = findViewById(R.id.repassword)

        dob!!.setOnFocusChangeListener { view, b ->
            if (b){
                val c = Calendar.getInstance()
                val year = c.get(Calendar.YEAR)
                val month = c.get(Calendar.MONTH)
                val day = c.get(Calendar.DAY_OF_MONTH)
                val datePickerDialog = DatePickerDialog(
                    this,
                    { view, year, monthOfYear, dayOfMonth ->
                        val dat = (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                        dob!!.setText(dat)
                        dob!!.clearFocus()
                    },
                    year,
                    month,
                    day
                )
                datePickerDialog.show()
                val calendar = Calendar.getInstance()
                calendar.add(Calendar.YEAR, -18)
                datePickerDialog.datePicker.maxDate = calendar.timeInMillis
            }
        }
        //val db = DBHelper(this, null)
        updtbtn!!.setOnClickListener {
            if (name!!.text.isEmpty()){
                name!!.setError("Please Enter Name")
                name!!.requestFocus()
            }else if (mobile!!.text.isEmpty()){
                mobile!!.setError("Please Enter Mobile Number")
                mobile!!.requestFocus()
            }else if (mobile!!.text.length != 10){
                mobile!!.setError("Mobile Number should be 10 digits")
                mobile!!.requestFocus()
            }else  if (email!!.text.isEmpty()){
                email!!.setError("Please Enter Email Address")
                email!!.requestFocus()
            }else if (!isValidEmail(email!!.text.toString())) {
                email!!.setError("Please valid email")
                email!!.requestFocus()
            }else if (dob!!.text.isEmpty()){
                dob!!.setError("Please select dob")
                dob!!.requestFocus()
            }else if (city!!.text.isEmpty()){
                city!!.setError("Please enter city")
                city!!.requestFocus()
            }/*else if (password!!.text.isEmpty()){
                password!!.setError("Please enter password")
                password!!.requestFocus()
            }else if (repassword!!.text.isEmpty()){
                repassword!!.setError("Please enter confirm password")
                repassword!!.requestFocus()
            }else if (!isValidPassword(password!!.text.toString())){
                password!!.setError("Enter valid password")
            }else if (!isValidPassword(repassword!!.text.toString())){
                repassword!!.setError("Enter valid confirm password")
            }else if (password!!.text.toString() != repassword!!.text.toString()){
                repassword!!.setError("password not match")
                repassword!!.requestFocus()
            }*/else{

                updateuser(email!!.text.toString()/*,AESEncyption.encrypt(password!!.text.toString())*/,name!!.text.toString(),mobile!!.text.toString(),
                    city!!.text.toString(),dob!!.text.toString())
            }

        }
        val getprofile = Getprofile()
        getprofile.execute(emailid)
    }

    private fun updateuser(
        email: String,
       /* password: String?,*/
        name: String,
        mobile: String,
        city: String,
        dob: String
    ) {
        var fullstring = "profilestatus='pending',newname='$name',newnumber='$mobile',newdob='$dob',newcity='$city'"
         val doupdate = Doupdate()
                doupdate.execute(email,fullstring)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home){
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
    private fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
    internal fun isValidPassword(password: String): Boolean {
        if (password.length < 8) return false
        if (password.filter { it.isDigit() }.firstOrNull() == null) return false
        if (password.filter { it.isLetter() }.filter { it.isUpperCase() }.firstOrNull() == null) return false
        if (password.filter { it.isLetter() }.filter { it.isLowerCase() }.firstOrNull() == null) return false
        if (password.filter { !it.isLetterOrDigit() }.firstOrNull() == null) return false

        return true
    }
    inner class Doupdate : AsyncTask<String?, String?, String?>() {

        var connectionClass: ConnectionClass? = ConnectionClass()
        var z = ""
        var isSuccess = false

        override fun onPreExecute() {

            progressDialog!!.setMessage("Loading...")
            progressDialog!!.show()
        }

        override fun doInBackground(vararg params: String?): String {
            try {
                val con: Connection = connectionClass!!.CONN()!!
                if (con == null) {
                    z = "Please check your internet connection"
                } else {
                    val query1 =
                        " update usertable set ${params[1]} where emptype='user' and email='${params[0]}'"
                    val stmt1 = con.createStatement()
                    val rs: Int = stmt1.executeUpdate(query1);
                    //val rs: ResultSet = stmt1.executeQuery(query1)
                    Log.d("TAG", "doInBackground: "+rs)
                    if (rs > 0){
                        isSuccess = true
                        z = "Your profile is sent for approval"
                    }else{
                        isSuccess = false
                        z = " update failed "
                    }

                }
            } catch (ex: Exception) {
                isSuccess = false
                z = "Exceptions$ex"
            }

            return z
        }

        override fun onPostExecute(s: String?) {
            if (isSuccess){
                onBackPressed()
            }
            Toast.makeText(this@profileedit, "" + z, Toast.LENGTH_LONG).show()
            Log.d("TAG", "onPostExecute: tttttttttttttt"+z.toString())
            progressDialog!!.hide()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (progressDialog != null ){
            progressDialog!!.dismiss()
            progressDialog = null
        }
    }

    inner class Getprofile : AsyncTask<String?, String?, String?>() {
        var approvals = Approvals()
        var connectionClass: ConnectionClass? = ConnectionClass()
        var z = ""
        var isSuccess = false

        override fun onPreExecute() {

            progressDialog!!.setMessage("Loading...")
            progressDialog!!.show()
        }

        override fun doInBackground(vararg params: String?): String {
            try {
                val con: Connection = connectionClass!!.CONN()!!
                if (con == null) {
                    z = "Please check your internet connection"
                } else {
                    val query1 =
                        " select * from usertable where emptype='user' and email='${params[0]}'"
                    val stmt1 = con.createStatement()
                    // stmt.executeUpdate(query);
                    val rs: ResultSet = stmt1.executeQuery(query1)
                    var i=0
                    while (rs.next()) {
                       i++
                        approvals.id = rs.getString(1)
                        approvals.email = rs.getString(2)
                        approvals.newpassword = rs.getString(3)
                        approvals.newname = rs.getString(4)
                        approvals.newnumber = rs.getString(5)
                        approvals.newcity = rs.getString(6)
                        approvals.newdob = rs.getString(7)
                        approvals.oldpassword = rs.getString(8)
                        approvals.oldname = rs.getString(9)
                        approvals.oldnumber = rs.getString(10)
                        approvals.oldcity = rs.getString(11)
                        approvals.olddob = rs.getString(12)
                        approvals.accountstatus = rs.getString(13)
                        approvals.profilestatus = rs.getString(14)
                        approvals.emptype = rs.getString(15)
                        approvals.createddate = rs.getString(16)

                    }
                    if (i >0){
                        isSuccess = true
                        z= "get profile"
                    }else{
                        isSuccess = false
                        z= "unable to get data"
                    }

                }
            } catch (ex: Exception) {
                isSuccess = false
                z = "Exceptions$ex"
            }

            return z
        }

        override fun onPostExecute(s: String?) {
            if (isSuccess){
                email!!.setText(approvals.email)
                name!!.setText(approvals.oldname)
                mobile!!.setText(approvals.oldnumber)
                dob!!.setText(approvals.olddob)
                city!!.setText(approvals.oldcity)
            }else {
                Toast.makeText(this@profileedit, "" + z, Toast.LENGTH_LONG).show()
            }
            Log.d("TAG", "onPostExecute: tttttttttttttt"+z.toString())
            progressDialog!!.hide()
        }
    }

}